<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Observer\Subscription\Save;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use RSHBIntech\Subscriptions\Api\Data\SubscriptionInterface;
use RSHBIntech\Subscriptions\Api\Helper\MailerInterface;
use RSHBIntech\Subscriptions\Enums\Db\SubscriptionType;

class After implements ObserverInterface
{
    /**
     * @param MailerInterface $mailer
     */
    public function __construct(
        private readonly MailerInterface $mailer,
    ) {
    }

    /**
     * @param Observer $observer
     * @event rshb_intech_subscription_save_after
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            // ! Only for testing purposes! Use cron job or queue instead!

            /** @var SubscriptionInterface $subscription */
            $subscription = $observer->getData('object');

            if (empty($subscription)) {
                return;
            }

            // TODO: replace on subscription notification sender with strategy pattern
            if (SubscriptionType::Email->value !== $subscription->getTypeId()) {
                $this->mailer->send($subscription->getValue());
            }
        } catch (\Throwable $e) {
            echo $e->getMessage();
        }
    }
}
