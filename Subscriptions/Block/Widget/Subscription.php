<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use RSHBIntech\Subscriptions\Model\Config\Source\Subscription\Types;

class Subscription extends Template implements BlockInterface
{
    /**
     * @param Template\Context $context
     * @param Types $types
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        private readonly Types $types,
        array $data = [],
    ) {
        parent::__construct($context, $data);
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return (int)$this->getData('enabled') === 1;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types->toOptionArray();
    }
}
