<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Model\Config\Source\Subscription;

use Magento\Framework\Data\OptionSourceInterface;
use RSHBIntech\Subscriptions\Enums\Db\SubscriptionType;

class Types implements OptionSourceInterface
{
    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        return [
            [
                'label' => __('Email'),
                'value' => SubscriptionType::Email->value,
            ],
            [
                'label' => __('Phone'),
                'value' => SubscriptionType::Phone->value,
            ],
            [
                'label' => __('Telegram'),
                'value' => SubscriptionType::Telegram->value,
            ],
            [
                'label' => __('Pigeon Mail'),
                'value' => SubscriptionType::PigeonMail->value,
            ],
        ];
    }
}
