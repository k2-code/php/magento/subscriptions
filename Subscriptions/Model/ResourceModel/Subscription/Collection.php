<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Model\ResourceModel\Subscription;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use RSHBIntech\Subscriptions\Model\Subscription;
use RSHBIntech\Subscriptions\Model\ResourceModel\Subscription as SubscriptionResource;

class Collection extends AbstractCollection
{
    /**
     * @return void
     */
    protected function _construct(): void
    {
        $this->_init(Subscription::class, SubscriptionResource::class);
    }
}
