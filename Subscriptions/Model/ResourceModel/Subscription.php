<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use RSHBIntech\Subscriptions\Api\IdAbleInterface;

class Subscription extends AbstractDb
{
    const TABLE_NAME = 'subscriptions';

    /**
     * @return void
     */
    protected function _construct(): void
    {
        $this->_init(static::TABLE_NAME, IdAbleInterface::ID);
    }
}
