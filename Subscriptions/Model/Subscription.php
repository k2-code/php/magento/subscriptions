<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Model;

use Magento\Framework\Model\AbstractModel;
use RSHBIntech\Subscriptions\Api\Data\SubscriptionInterface;
use RSHBIntech\Subscriptions\Model\ResourceModel\Subscription as SubscriptionResource;

class Subscription extends AbstractModel implements SubscriptionInterface
{
    protected $_eventPrefix = 'rshb_intech_subscription';
    protected $_idFieldName = self::ID;

    /**
     * @return void
     */
    protected function _construct(): void
    {
        $this->_init(SubscriptionResource::class);
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return (int)$this->getData(static::TYPE_ID);
    }

    /**
     * @param int $typeId
     * @return $this
     */
    public function setTypeId(int $typeId): static
    {
        return $this->setData(static::TYPE_ID, $typeId);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->getData(static::VALUE);
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): static
    {
        return $this->setData(static::VALUE, $value);
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->getData(static::CREATED_AT);
    }

    /**
     * @param string $createdAt
     * @return $this
     */
    public function setCreatedAt(string $createdAt): static
    {
        return $this->setData(static::CREATED_AT, $createdAt);
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->getData(static::UPDATED_AT);
    }

    /**
     * @param string $updatedAt
     * @return $this
     */
    public function setUpdatedAt(string $updatedAt): static
    {
        return $this->setData(static::UPDATED_AT, $updatedAt);
    }
}
