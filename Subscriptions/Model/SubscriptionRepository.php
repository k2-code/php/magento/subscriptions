<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use RSHBIntech\Subscriptions\Api\Data\SubscriptionInterface;
use RSHBIntech\Subscriptions\Api\Data\SubscriptionSearchResultsInterface;
use RSHBIntech\Subscriptions\Api\Data\SubscriptionSearchResultsInterfaceFactory;
use RSHBIntech\Subscriptions\Api\SubscriptionRepositoryInterface;
use RSHBIntech\Subscriptions\Model\ResourceModel\Subscription as SubscriptionResource;
use RSHBIntech\Subscriptions\Model\ResourceModel\Subscription\Collection as SubscriptionCollection;
use RSHBIntech\Subscriptions\Model\ResourceModel\Subscription\CollectionFactory as SubscriptionCollectionFactory;
use RSHBIntech\Subscriptions\Model\SubscriptionFactory;

class SubscriptionRepository implements SubscriptionRepositoryInterface
{
    /**
     * @param SubscriptionResource $resource
     * @param SubscriptionFactory $factory
     * @param SubscriptionCollectionFactory $collectionFactory
     * @param SubscriptionSearchResultsInterfaceFactory $searchResultFactory
     * @param array $registry
     */
    public function __construct(
        private readonly SubscriptionResource $resource,
        private readonly SubscriptionFactory $factory,
        private readonly SubscriptionCollectionFactory $collectionFactory,
        private readonly SubscriptionSearchResultsInterfaceFactory $searchResultFactory,
        private array $registry = [],
    ){

    }

    /**
     * @param int $id
     * @return SubscriptionInterface
     * @throws NoSuchEntityException
     */
    public function get(int $id): SubscriptionInterface
    {
        if (! empty($this->registry[$id])) {
            return $this->registry[$id];
        }

        /** @var SubscriptionInterface $subscription */
        $subscription = $this->factory->create();

        $this->resource->load($subscription, $id);

        if ($subscription->getId()) {
            return $this->registry[$id] = $subscription;
        }

        throw new NoSuchEntityException(__("Requested subscription ($id) does not exists"));
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return SubscriptionSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SubscriptionSearchResultsInterface
    {
        /** @var SubscriptionCollection $collection */
        $collection = $this->collectionFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $collection->addFieldToFilter($filter->getField(), [
                    $filter->getConditionType() => $filter->getValue(),
                ]);
            }
        }

        /** @var SubscriptionSearchResultsInterface $searchResult */
        $searchResult = $this->searchResultFactory->create();

        $searchResult
            ->setSearchCriteria($searchCriteria)
            ->setItems($collection->getItems())
            ->setTotalCount($collection->getSize())
        ;

        return $searchResult;
    }

    /**
     * @param SubscriptionInterface $subscription
     * @return SubscriptionInterface
     * @throws StateException
     */
    public function save(SubscriptionInterface $subscription): SubscriptionInterface
    {
        $id = $subscription->getId();

        try {
            $this->resource->save($subscription);

            if (empty($this->registry[$id])) {
                // TODO: load model for timestamps info ?
                return $this->registry[$id] = $subscription;
            }

        } catch (\Throwable) {
            throw new StateException(__('Unable to save subscription'));
        }

        return $this->registry[$id];
    }

    /**
     * @param SubscriptionInterface $subscription
     * @return bool
     * @throws StateException
     */
    public function delete(SubscriptionInterface $subscription): bool
    {
        $id = $subscription->getId();

        try {
            $this->resource->delete($subscription);
            unset($this->registry[$id]);
        } catch (\Throwable) {
            throw new StateException(__('Unable to delete subscription #%1', $id));
        }

        return true;
    }

    /**
     * @param int $id
     * @return bool
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function deleteById(int $id): bool
    {
        return $this->delete($this->get($id));
    }
}
