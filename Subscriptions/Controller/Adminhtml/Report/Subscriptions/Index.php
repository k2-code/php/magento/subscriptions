<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Controller\Adminhtml\Report\Subscriptions;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class Index extends Action implements HttpGetActionInterface
{
    /**
     * Authorization level of a basic admin session
     */
    public const ADMIN_RESOURCE = 'RSHBIntech_Subscriptions::report_subscriptions';

    /**
     * Execute action based on request and return result
     *
     * @return ResultInterface | ResponseInterface
     */
    public function execute()
    {
        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        try {
            $this->_setActiveMenu('Magento_Reports::report');

            $page
                ->getConfig()
                ->getTitle()
                ->prepend(__('Subscriptions Report'))
            ;

            return $page;
        } catch (\Throwable $e) {
            echo $e->getMessage();
        }

        return $page;
    }
}
