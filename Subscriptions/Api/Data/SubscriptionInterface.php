<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Api\Data;

use RSHBIntech\Subscriptions\Api\IdAbleInterface;
use RSHBIntech\Subscriptions\Api\TimestampAbleInterface;

interface SubscriptionInterface extends IdAbleInterface, TimestampAbleInterface
{
    const TYPE_ID = 'type_id';
    const VALUE = 'value';

    /**
     * @return int
     */
    public function getTypeId(): int;

    /**
     * @param int $typeId
     * @return $this
     */
    public function setTypeId(int $typeId): static;

    /**
     * @return string
     */
    public function getValue(): string;

    /**
     * @param string $value
     * @return $this
     */
    public function setValue(string $value): static;
}
