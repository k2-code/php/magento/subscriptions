<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface SubscriptionSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \RSHBIntech\Subscriptions\Api\Data\SubscriptionInterface[]
     */
    public function getItems(): array;

    /**
     * @param \RSHBIntech\Subscriptions\Api\Data\SubscriptionInterface[] $items
     * @return $this
     */
    public function setItems(array $items = null): static;
}
