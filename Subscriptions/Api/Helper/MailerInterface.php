<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Api\Helper;

interface MailerInterface
{
    /**
     * @param string $to
     * @return bool
     */
    public function send(string $to): bool;
}
