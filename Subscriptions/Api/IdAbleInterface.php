<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Api;

interface IdAbleInterface
{
    const ID = 'id';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);
}
