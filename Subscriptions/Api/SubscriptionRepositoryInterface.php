<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use RSHBIntech\Subscriptions\Api\Data\SubscriptionInterface;
use RSHBIntech\Subscriptions\Api\Data\SubscriptionSearchResultsInterface;

/**
 * Working with subscriptions via REST API
 */
interface SubscriptionRepositoryInterface
{
    /**
     * @param int $id
     * @return \RSHBIntech\Subscriptions\Api\Data\SubscriptionInterface
     */
    public function get(int $id): SubscriptionInterface;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \RSHBIntech\Subscriptions\Api\Data\SubscriptionSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): SubscriptionSearchResultsInterface;

    /**
     * @param \RSHBIntech\Subscriptions\Api\Data\SubscriptionInterface $subscription
     * @return \RSHBIntech\Subscriptions\Api\Data\SubscriptionInterface
     */
    public function save(SubscriptionInterface $subscription): SubscriptionInterface;

    /**
     * @param \RSHBIntech\Subscriptions\Api\Data\SubscriptionInterface $subscription
     * @return bool
     */
    public function delete(SubscriptionInterface $subscription): bool;

    /**
     * @param int $id
     * @return bool
     */
    public function deleteById(int $id): bool;
}
