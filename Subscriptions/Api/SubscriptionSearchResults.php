<?php

namespace RSHBIntech\Subscriptions\Api;

use Magento\Framework\Api\AbstractSimpleObject;
use Magento\Framework\Api\SearchCriteria;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Model\AbstractExtensibleModel;
use RSHBIntech\Subscriptions\Api\Data\SubscriptionSearchResultsInterface;

/**
 * @notice Added full copy of class because getList method is not working with parent
 * interface and doesn't convert loaded models from collection to result items array
 */
class SubscriptionSearchResults extends AbstractSimpleObject implements SubscriptionSearchResultsInterface
{
    const KEY_ITEMS = 'items';
    const KEY_SEARCH_CRITERIA = 'search_criteria';
    const KEY_TOTAL_COUNT = 'total_count';

    /**
     * Get items
     *
     * @return AbstractExtensibleModel[]
     */
    public function getItems(): array
    {
        return $this->_get(self::KEY_ITEMS) ?? [];
    }

    /**
     * Set items
     *
     * @param AbstractExtensibleModel[] $items
     * @return $this
     */
    public function setItems(array $items = null): static
    {
        return $this->setData(self::KEY_ITEMS, $items);
    }

    /**
     * Get search criteria
     *
     * @return SearchCriteria
     */
    public function getSearchCriteria()
    {
        return $this->_get(static::KEY_SEARCH_CRITERIA);
    }

    /**
     * Set search criteria
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return $this
     */
    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria)
    {
        return $this->setData(static::KEY_SEARCH_CRITERIA, $searchCriteria);
    }

    /**
     * Get total count
     *
     * @return int
     */
    public function getTotalCount()
    {
        return $this->_get(static::KEY_TOTAL_COUNT);
    }

    /**
     * Set total count
     *
     * @param int $count
     * @return $this
     */
    public function setTotalCount($count)
    {
        return $this->setData(static::KEY_TOTAL_COUNT, $count);
    }
}
