<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Console\Command;

use RSHBIntech\Subscriptions\Api\Data\SubscriptionInterface;
use RSHBIntech\Subscriptions\Api\SubscriptionRepositoryInterface;
use RSHBIntech\Subscriptions\Enums\Db\SubscriptionType;
use RSHBIntech\Subscriptions\Model\SubscriptionFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateSubscription extends Command
{
    const ARGUMENT_TYPE = 'type';
    const ARGUMENT_VALUE = 'value';

    /**
     * @param SubscriptionFactory $factory
     * @param SubscriptionRepositoryInterface $repository
     * @param string|null $name
     */
    public function __construct(
        private readonly SubscriptionFactory $factory,
        private readonly SubscriptionRepositoryInterface $repository,
        string $name = null,
    ) {
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('rshbintech:subscriptions:create')
            ->setDescription('Creating new subscription')
            ->setDefinition([
                new InputArgument(
                    name: static::ARGUMENT_TYPE,
                    mode: InputArgument::REQUIRED,
                    description: sprintf(
                        "Subscription type. Email (%s), Phone (%s), Telegram (%s)",
                        SubscriptionType::Email->value,
                        SubscriptionType::Phone->value,
                        SubscriptionType::Telegram->value,
                    ),
                ),

                new InputArgument(
                    name: static::ARGUMENT_VALUE,
                    mode: InputArgument::REQUIRED,
                    description: 'Subscription value',
                ),
            ]);

        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type  = $input->getArgument(static::ARGUMENT_TYPE);
        $value = $input->getArgument(static::ARGUMENT_VALUE);

        /** @var SubscriptionInterface $subscription */
        $subscription = $this->factory->create();

        $subscription
            ->setTypeId((int)$type)
            ->setValue($value)
        ;

        $this->repository->save($subscription);

        if ($output->getVerbosity() > 1) {
            $output->writeln("<info>Subscription with ID {$subscription->getId()} was created!</info>");
        }
    }
}
