<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Helper;

use Magento\Framework\App\Area;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\MailException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;
use RSHBIntech\Subscriptions\Api\Helper\MailerInterface;

class Mailer extends AbstractHelper implements MailerInterface
{
    public function __construct(
        Context                                $context,
        private readonly TransportBuilder      $transportBuilder,
        private readonly StoreManagerInterface $storeManager,
        private readonly StateInterface        $translation,
    ) {
        parent::__construct($context);
    }

    /**
     * @param string $to
     * @return bool
     *
     * @throws LocalizedException
     * @throws MailException
     * @throws NoSuchEntityException
     */
    public function send(string $to): bool
    {
        $templateID = $this->scopeConfig->getValue('rshb_intech_subscriptions/general/template_id', ScopeInterface::SCOPE_STORE);
        $fromEmail = $this->scopeConfig->getValue('trans_email/ident_support/email', ScopeInterface::SCOPE_STORE);
        $fromName = $this->scopeConfig->getValue('trans_email/ident_support/name', ScopeInterface::SCOPE_STORE);

        if (empty($templateID)) {
            return false;
        }

        if (empty($fromEmail)) {
            return false;
        }

        if (empty($fromName)) {
            return false;
        }

        $from = [
            'email' => $fromEmail,
            'name' => $fromName,
        ];

        $templateVars = [
            'email' => $to,
        ];

        $storeID = $this->storeManager->getStore()->getId();

        $templateOptions = [
            'area'  => Area::AREA_FRONTEND,
            'store' => $storeID,
        ];

        $this->translation->suspend();

        $transport = $this->transportBuilder
            ->setTemplateIdentifier($templateID)
            ->setTemplateOptions($templateOptions)
            ->setTemplateVars($templateVars)
            ->setFromByScope($from, $storeID)
            ->addTo($to)
            ->getTransport()
        ;

        $transport->sendMessage();
        $this->translation->resume();

        return true;
    }
}
