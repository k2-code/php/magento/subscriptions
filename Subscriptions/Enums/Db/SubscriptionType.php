<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Enums\Db;

enum SubscriptionType: int
{
    case Email = 1;
    case Phone = 2;
    case Telegram = 3;
    case PigeonMail = 4;
}
