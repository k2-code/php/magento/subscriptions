<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;
use RSHBIntech\Subscriptions\Setup\Traits\{PrimaryKeyAble, TimestampAble};
use RSHBIntech\Subscriptions\Enums\Db\SubscriptionType;
use Zend_Db_Exception;

class InstallSchema implements InstallSchemaInterface
{
    use PrimaryKeyAble;
    use TimestampAble;

    private SchemaSetupInterface $setup;
    private AdapterInterface $connection;

    /**
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context): void
    {
        $this->setup = $setup;
        $this->connection = $setup->getConnection();

        try {
            $setup->startSetup();

            $this->installSubscriptionTypes();
            $this->installSubscriptions();

            $this->fillSubscriptionTypes();

            $setup->endSetup();
        } catch (\Throwable $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @return void
     * @throws Zend_Db_Exception
     */
    private function installSubscriptionTypes(): void
    {
        $table = 'subscription_types';

        if ($this->setup->tableExists($table)) {
            return;
        }

        $table = $this->connection->newTable($table)
            ->addColumn(
                name: 'title',
                type: Table::TYPE_TEXT,
                size: 30,
                options: [
                    'nullable' => false,
                ],
                comment: 'Subscription type title',
            )
            ->setComment('Subscription types table')
        ;

        $this
            ->addPrimaryKey($table)
            ->addTimestamps($table)
        ;

        $this->connection->createTable($table);
    }

    /**
     * @return void
     * @throws Zend_Db_Exception
     */
    private function installSubscriptions(): void
    {
        $table = 'subscriptions';

        if ($this->setup->tableExists($table)) {
            return;
        }

        $table = $this->setup->getConnection()->newTable($table)
            ->addColumn(
                name: 'type_id',
                type: Table::TYPE_INTEGER,
                size: 1,
                options: [
                    'nullable' => false,
                    'unsigned' => true,
                ],
                comment: 'Subscription type ID',
            )
            ->addColumn(
                name: 'value',
                type: Table::TYPE_TEXT,
                size: 100,
                options: [
                    'nullable' => false,
                ],
                comment: 'Subscription value',
            )
            ->addForeignKey(
                fkName: 's_st_type_id_fk',
                column: 'type_id',
                refTable: 'subscription_types',
                refColumn: 'id',
            )
            ->addIndex(
                indexName: "{$table}_type_id_idx",
                fields: 'type_id',
            )
            ->addIndex(
                indexName: "{$table}_value_idx",
                fields: 'value',
                options: [
                    'type' => AdapterInterface::INDEX_TYPE_UNIQUE,
                ],
            )
        ;

        $this
            ->addPrimaryKey($table)
            ->addTimestamps($table)
        ;

        $this->connection->createTable($table);
    }

    /**
     * @return void
     */
    private function fillSubscriptionTypes(): void
    {
        $table = 'subscription_types';

        if (! $this->setup->tableExists($table)) {
            return;
        }

        $this->connection->insertMultiple($table, [
            ['title' => SubscriptionType::Email->name],
            ['title' => SubscriptionType::Phone->name],
            ['title' => SubscriptionType::Telegram->name],
            ['title' => SubscriptionType::PigeonMail->name],
        ]);
    }
}
