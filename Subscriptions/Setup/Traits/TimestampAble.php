<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Setup\Traits;

use Magento\Framework\DB\Ddl\Table;
use Zend_Db_Exception;

trait TimestampAble
{
    /**
     * @throws Zend_Db_Exception
     */
    public function addTimestamps(Table $table): static
    {
        $table
            ->addColumn(
                name: 'created_at',
                type: Table::TYPE_TIMESTAMP,
                options: [
                    'nullable' => false,
                    'default'  => Table::TIMESTAMP_INIT,
                ],
                comment: 'Created At',
            )
            ->addColumn(
                name: 'updated_at',
                type: Table::TYPE_TIMESTAMP,
                options: [
                    'nullable' => false,
                    'default'  => Table::TIMESTAMP_INIT_UPDATE,
                ],
                comment: 'Updated At',
            )
        ;

        return $this;
    }
}
