<?php
declare(strict_types=1);

namespace RSHBIntech\Subscriptions\Setup\Traits;

use Magento\Framework\DB\Ddl\Table;
use Zend_Db_Exception;

trait PrimaryKeyAble
{
    /**
     * @throws Zend_Db_Exception
     */
    public function addPrimaryKey(Table $table): static
    {
        $table
            ->addColumn(
                name: 'id',
                type: Table::TYPE_INTEGER,
                options: [
                    'identity' => true,
                    'primary'  => true,
                    'unsigned' => true,
                    'nullable' => false,
                ],
                comment: "ID",
            )
        ;

        return $this;
    }
}
